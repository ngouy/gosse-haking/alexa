/*
** Requires
*/

const my_http = require("http");
const url = require("url");
var alexaData = require('alexa-traffic-rank');

/*
** Constants
*/

/*
** Core functions
*/

// response functions
error_func = (response, error_message) => {
  // response.writeHeader(400, {"Content-Type": "application/json"});
  console.log(JSON.stringify({error: error_message || "Unknow enpoint"}));
  // response.end();
};

ok_func = (object_response, response) => {
//   response.writeHeader(200, {"Content-Type": "application/json"});
   console.log(JSON.stringify(object_response));
//   response.end();
}

// domain extractor
extractHostname = url => {
    var hostname;
    if (url.indexOf("://") > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }
    hostname = hostname.split(':')[0].split('www.');
    hostname = hostname[1] || hostname[0];
    return hostname;
}



// main function
crawl_me = (query_params, http_response) => {
  var domain = extractHostname(query_params.domain || query_params.unknown || "");
  var url = query_params.url || `http://${domain}`;
  alexaData.AlexaWebData(url, function(error, result) {
    // console.log({ 'domain': domain, 'global_rank': result.globalRank, 'country': result.countryRank.country, 'country_rank': result.countryRank.rank
    console.log(`'domain': |${domain}|, 'global_rank': |${result.globalRank}|, 'country': |${result.countryRank.country}|, 'country_rank': |${result.countryRank.rank}|`);
  });
}

process.argv.forEach(function (val, index, array) {
  if (index > 1) {
    var params = JSON.parse(val);
    crawl_me(params, null);
  }
});

// console.log("Server Running on 5020");